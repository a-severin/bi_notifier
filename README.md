# BiNotifier

���������� �������:

* Commons - ���������� ����� �������
* ConfigurationApplication - WPF Descktop Application ��� ��������� ������ �����������
* JobProcessor - ����������-���������� ��� Autodesk Vault Job Processor'�
* Setup - ������ ��� ��������� msi-�����������
* Tests - �������� ������

## ConfigurationApplication

WPF ���������� ����������� �� ������� MVVM. ������������ �����������:
* Prism - ��� ������� MVVM
* Syncfusion - UI ����������

��������� ������ �� Vault �������������� � �����:
* IVaultProvider - ���������� ����������� �������� ��� ������
* VaultProvider - ����� ������ ��������� ������
* GuiValutProvider - ���������� ���������� ����������� � Vault - ����� ������� ������

## JobProcessor

��� ����������� ������������ ���������� Microsoft.Practices.EnterpriseLibrary.Logging ������� ���� � ������� Autodesk Vault.

������ ������ ������������ ��� �������� ������� ������. ����� ���������� ������� �������� � ���� ���������� ������������� Job Processor.


