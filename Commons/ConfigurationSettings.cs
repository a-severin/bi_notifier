﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Polenter.Serialization;

namespace Commons {
    /// <summary>
    /// Настройки приложения, включая: настйроки почтового сервера, настройки даыннх в Vault
    /// </summary>
    [Serializable]
    public class ConfigurationSettings {
        /// <summary>
        /// Загружает настройки из общедоступного каталога приложений, если таковыйх нет, то создает новый объект со значениями по умолчнию
        /// </summary>
        public static ConfigurationSettings Load() {
            var path = _getPath();
            if (!File.Exists(path)) {
                return new ConfigurationSettings();
            }

            var serializer = new SharpSerializer();
            return (ConfigurationSettings) serializer.Deserialize(path);
        }

        /// <summary>
        /// Возвращает путь для настроек, создает при необходимости каталог компании в общедоступном каталоге приложений
        /// </summary>
        private static string _getPath() {
            var pd = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            var comp = pd + Path.DirectorySeparatorChar + "BI-Group";
            if (!Directory.Exists(comp)) {
                Directory.CreateDirectory(comp);
            }

            var path = comp + Path.DirectorySeparatorChar + "ConfigurationSettings.xml";
            return path;
        }

        /// <summary>
        /// Id выбранных для уведомления Vault групп
        /// </summary>
        public StringCollection SelectedGroups { get; set; } = new StringCollection();

        /// <summary>
        /// Коллекция кодов задающих доступ к обновлениям в каталоге: f{id каталога}g{id группы} - доступ предоставляется группе или f{id каталога}s - доступ предосталвяется только пользователям с правом на редактирование
        /// </summary>
        public StringCollection GroupAccess { get; set; } = new StringCollection();

        /// <summary>
        /// Адрес почтового сервера, значение по умолчанию - localhost
        /// </summary>
        public string SmtpHost { get; set; } = "localhost";

        /// <summary>
        /// Адрес отправителя
        /// </summary>
        public string FromEmail { get; set; } = string.Empty;

        /// <summary>
        /// Порт почтового сервера, по умолчанию 25
        /// </summary>
        public int SmtpPort { get; set; } = 25;

        /// <summary>
        /// Заголовок письма, по умолчанию "Vault Update"
        /// </summary>
        public string EmailTitle { get; set; } = "Vault Update";

        /// <summary>
        /// Пароль к почтовому серверу
        /// </summary>
        public string SmtpPassword { get; set; } = string.Empty;

        /// <summary>
        /// Логин к почтовому серверу
        /// </summary>
        public string SmtpUsername { get; set; } = string.Empty;

        /// <summary>
        /// Путь к корневому каталогу BIM-шаблона, по умолчанию "$/0_Стандарты и правила/0.1._BIM-стандарты/0.1.3_BIM-стандарт/0.1.3.1_Структура папок земельного участка"
        /// </summary>
        public string RootPath { get; set; } = @"$/0_Стандарты и правила/0.1._BIM-стандарты/0.1.3_BIM-стандарт/0.1.3.1_Структура папок земельного участка";

        /// <summary>
        /// Префикс для распознавания рабочей группы, по умолчанию "ГРП"
        /// </summary>
        public string GroupPrefix { get; set; } = "ГРП";

        /// <summary>
        /// Сохранить настройки в общедоступный каталог приложений
        /// </summary>
        public void Save() {
            var path = _getPath();
            var serializer = new SharpSerializer();
            serializer.Serialize(this, path);
        }
    }
}