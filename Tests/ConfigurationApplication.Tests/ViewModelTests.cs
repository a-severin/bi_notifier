﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Connectivity.WebServices;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.DataManagement.Client.Framework.Vault.Results;
using ConfigurationApplication.Properties;
using ConfigurationApplication.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConfigurationApplication.Tests {
    [TestClass]
    public class ViewModelTests {
        [TestMethod]
        public void Constructor_InitCommands() {
            var vm = new ViewModel(new FakeVaultProvider(), new Commons.ConfigurationSettings());
            Assert.IsNotNull(vm.Connect);
            Assert.IsNotNull(vm.LoadFolders);
            Assert.IsNotNull(vm.GroupPresenters);
        }

        [TestMethod]
        public void Connect_CallLogin_OnExecute() {
            var provider = new FakeVaultProvider();
            var vm = new ViewModel(provider, new Commons.ConfigurationSettings());
            vm.Connect.Execute();
            Assert.IsTrue(provider.LoginCalled);
        }

        [TestMethod]
        public void LoadFolders_CallGetRootFolder_OnExecute() {
            var provider = new FakeVaultProvider();
            var vm = new ViewModel(provider, new Commons.ConfigurationSettings()) {RootPath = "test"};
            vm.GroupPresenters.Add(new GroupPresenter(new Group()));
            vm.LoadFolders.Execute();
            Assert.IsTrue(provider.GetRootFolderCalled);
        }

        [TestMethod]
        public void LoadFolders_CallGetChildrenFolders_OnExecute() {
            var provider = new FakeVaultProvider();
            var vm = new ViewModel(provider, new Commons.ConfigurationSettings()) { RootPath = "test"};
            vm.GroupPresenters.Add(new GroupPresenter(new Group()));

            vm.LoadFolders.Execute();

            Assert.IsTrue(provider.GetChildrenFoldersCalled);
        }

        [TestMethod]
        public void LoadFolders_CreateFolderPresenters_OnExecute() {
            var provider = new FakeVaultProvider();
            var vm = new ViewModel(provider, new Commons.ConfigurationSettings()) { RootPath = "test" };
            vm.GroupPresenters.Add(new GroupPresenter(new Group()));

            vm.LoadFolders.Execute();
           
            Assert.IsNotNull(vm.FolderPresenters);
            Assert.IsTrue(vm.FolderPresenters.Any());
        }

        [TestMethod]
        public void LoadFolders_CanExecute_AfterLoginAndSetRootPath() {
            var provider = new FakeVaultProvider();
            provider.Login();
            var vm = new ViewModel(provider, new Commons.ConfigurationSettings()) { RootPath = string.Empty };
            vm.GroupPresenters.Add(new GroupPresenter(new Group()));

            Assert.IsFalse(vm.LoadFolders.CanExecute());

            vm.RootPath = "test";
            Assert.IsTrue(vm.LoadFolders.CanExecute());
        }

    }

    public class FakeVaultProvider : IVaultProvider {
        public bool LoginCalled = false;
        public void Login() {
            LoginCalled = true;
            Connected = true;
        }

        public bool Connected { get; private set; }

        public void Logout() {
            throw new NotImplementedException();
        }

        public bool GetRootFolderCalled = false;
        public bool GetChildrenFoldersCalled;

        public FolderPresenter GetRootFolder(string path) {
            GetRootFolderCalled = true;
            return new FolderPresenter(new Folder{Id = 1}, null);
        }

        public IEnumerable<FolderPresenter> GetChildrenFolders(FolderPresenter parent) {
            GetChildrenFoldersCalled = true;
            return Enumerable.Empty<FolderPresenter>();
        }

        public IEnumerable<GroupPresenter> GetGroups() {
            throw new NotImplementedException();
        }
    }
}
