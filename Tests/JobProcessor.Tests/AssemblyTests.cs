﻿using System;
using System.Linq;
using System.Reflection;
using Autodesk.Connectivity.Extensibility.Framework;
using BiNotifier;
using BiNotifier.JobProcessor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JobProcessor.Tests {
    [TestClass]
    public class AssemblyTests {
        [TestMethod]
        public void AssemblyCompanyAdded() {
            var assembly = typeof(ProcessorJobHandler).Assembly;
            Assert.IsTrue(
                assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), true).Any(_ => !string.IsNullOrEmpty((_ as AssemblyCompanyAttribute)?.Company)));
        }

        [TestMethod]
        public void AssemblyProductAdded() {
            var assembly = typeof(ProcessorJobHandler).Assembly;
            Assert.IsTrue(
                assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), true).Any(_ => !string.IsNullOrEmpty((_ as AssemblyProductAttribute)?.Product)));
        }

        [TestMethod]
        public void AssemblyDescriptionAdded() {
            var assembly = typeof(ProcessorJobHandler).Assembly;
            Assert.IsTrue(
                assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), true)
                    .Any(_ => !string.IsNullOrEmpty((_ as AssemblyDescriptionAttribute)?.Description)));
        }

        [TestMethod]
        public void ExtensionIdAdded() {
            var assembly = typeof(ProcessorJobHandler).Assembly;
            Assert.IsTrue(
                assembly.GetCustomAttributes(typeof(ExtensionIdAttribute), true).Any(_ => ((ExtensionIdAttribute)_).Id != Guid.Empty));
        }

        [TestMethod]
        public void ApiVersionAdded() {
            var assembly = typeof(ProcessorJobHandler).Assembly;
            Assert.IsTrue(
                assembly.GetCustomAttributes(typeof(ApiVersionAttribute), true).Any(_ => !string.IsNullOrEmpty((_ as ApiVersionAttribute)?.Version)));
        }
    }
}
