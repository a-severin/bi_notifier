﻿using System;
using BiNotifier.JobProcessor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JobProcessor.Tests {
    [TestClass]
    public class UtilityTests {
        [TestMethod]
        public void TestParseLand() {
            Assert.AreEqual("Химки", "$/Химки/4_Проекты/1_Бизнес-центр".ParseLand());
        }

        [TestMethod]
        public void TestParseProjectName() {
            Assert.AreEqual("1_Бизнес-центр", "$/Химки/4_Проекты/1_Бизнес-центр/4_ПСД и ПИР/4.8_РП".ParseProjectName());
        }
    }
}
