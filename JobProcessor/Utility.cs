using System;
using System.Text.RegularExpressions;

public static class Utility {
    /// <summary>
    /// ��������� �� ���� � �������� �������� ���������� �������, ��� ������ ����� ������ � ������ ��������� '/'
    /// </summary>
    public static string ParseLand(this string folderFullName) {
        var land = folderFullName.Remove(0, folderFullName.IndexOf("/", StringComparison.InvariantCulture) + 1);
        land = land.Remove(land.IndexOf("/", StringComparison.InvariantCulture));
        return land;
    }

    /// <summary>
    /// ��������� �� ���� � �������� �������� �������, ��� ������ ����� "4_�������/" � ����������� "/"
     /// </summary>
    public static string ParseProjectName(this string folderFullName) {
        const string flag = "4_�������";
        if (!folderFullName.Contains(flag)) {
            return string.Empty;
        }

        var projectName = folderFullName.Remove(0, folderFullName.IndexOf(flag, StringComparison.InvariantCulture) + flag.Length + 1);

        return projectName.Remove(projectName.IndexOf("/", StringComparison.InvariantCulture));
    }
}