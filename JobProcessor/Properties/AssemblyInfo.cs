﻿using System.Reflection;
using System.Runtime.InteropServices;
using Autodesk.Connectivity.Extensibility.Framework;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BiNotifier JobProcessor")]
[assembly: AssemblyDescription("Notify by group")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("BI-Group")]
[assembly: AssemblyProduct("BiNotifier")]
[assembly: AssemblyCopyright("Copyright © 2017 BI-Group")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("429576c3-84b0-47a0-98bd-2052326b303a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

#if V2017
[assembly: ApiVersion("10.0")]
#endif
#if V2018
[assembly: ApiVersion("11.0")]
#endif
[assembly: ExtensionId("D1E6C084-5B0A-4A14-81A9-BAAF40FAD742")]