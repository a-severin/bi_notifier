﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Entities;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Properties;
using Commons;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using ACJE = Autodesk.Connectivity.JobProcessor.Extensibility;
using File = Autodesk.Connectivity.WebServices.File;
using Folder = Autodesk.Connectivity.WebServices.Folder;
using Group = Autodesk.Connectivity.WebServices.Group;
using VDF = Autodesk.DataManagement.Client.Framework;

namespace BiNotifier.JobProcessor {
    public class ProcessorJobHandler : ACJE.IJobHandler {
        /// <summary>
        ///     шаблон пись запоминается при первом считывании
        /// </summary>
        private string _body;

        public ACJE.JobOutcome Execute(ACJE.IJobProcessorServices context, ACJE.IJob job) {
            Logger.Write($"JobHandler Execute {job.JobType} {job.Id} {job.Description}");
            try {
                // загружаем насройки приложения
                var config = ConfigurationSettings.Load();

                var wsm = context.Connection.WebServiceManager;

                // получаем все группы в виде словаря по Id
                var groupMap = wsm.AdminService.GetAllGroups().ToDictionary(_ => _.Id, _ => _);
                Logger.Write($"Groups: {string.Join(", ", groupMap.Values.Select(_ => $"{_.Name}({_.Id})"))}");

                // парсим Id файла для которого был вызван Job
                var fileId = Convert.ToInt64(job.Params["EntityId"]);
                // получаем файл из Vault
                var file = wsm.DocumentService.GetFileById(fileId);

                // получаем каталог файла
                var folder = wsm.DocumentService.GetFolderById(file.FolderId);

                context.Log($"File placed in folder: {folder.FullName}", ACJE.MessageType.eInformation);

                // загружает из настроек приложения правила доступа к каталогам BIM-шаблона
                var configAccessMap = new Dictionary<long, FolderGroupAccess>();
                foreach (var key in config.GroupAccess) {
                    var folderId = FolderGroupAccess.ParseFolderId(key);
                    FolderGroupAccess fga;
                    if (!configAccessMap.TryGetValue(folderId, out fga)) {
                        try {
                            var gaFolder = wsm.DocumentService.GetFolderById(folderId);
                            if (gaFolder == null) {
                                continue;
                            }
                            fga = new FolderGroupAccess {FolderId = folderId, Folder = gaFolder};
                            configAccessMap[folderId] = fga;
                        } catch (Exception) {
                            continue;
                        }
                    }
                    if (FolderGroupAccess.IsWriteOnly(key)) {
                        fga.WriteOnlyAccess = true;
                    } else {
                        if (groupMap.TryGetValue(FolderGroupAccess.ParseGroupId(key), out var vGroup)) {
                            fga.Groups.Add(vGroup);
                        }
                    }
                }

                // ищет для каталога, в катором лежит файл, правило доступа, сравнивая имя каталога, и подымаясь вверх по каталогам
                FolderGroupAccess findGroupAccess(IEnumerable<FolderGroupAccess> fgss, Folder target) {
                    try {
                        var fgs = fgss.FirstOrDefault(_ => _.Folder.Name.Equals(target.Name, StringComparison.InvariantCulture));
                        if (fgs == null) {
                            if (target.ParId < 0) {
                                return null;
                            }

                            var parent = wsm.DocumentService.GetFolderById(target.ParId);
                            if (parent == null) {
                                return null;
                            }
                            return findGroupAccess(fgss, parent);
                        }
                        return fgs;
                    } catch (Exception e) {
                        context.Log(e, $"Unexcpeted exception occure in findGroupAccess\n{e}");
                        return null;
                    }
                }

                // получаем правило доступа, в противном случае завершаем работу
                var configuredAccesses = findGroupAccess(configAccessMap.Values, folder);
                if (configuredAccesses == null) {
                    context.Log("GroupAccess not founded", ACJE.MessageType.eInformation);
                    return ACJE.JobOutcome.Success;
                }
                context.Log(
                    $"Configured Access WriteOnlyAccess: {configuredAccesses.WriteOnlyAccess}, for groups: {string.Join(", ", configuredAccesses.Groups.Select(_ => _.Name))}",
                    ACJE.MessageType.eInformation);

                // вычисляем список пользователей, которых нужно уведомить об изменениях в файле
                var usersToNotify = new Dictionary<User, bool>();

                // загружаем из Vault список рабочих групп
                var accessGroupd = wsm.SecurityService.GetEntACLsByEntityIds(new[] {folder.Id});
                Logger.Write($"Access groups to folder {folder.Name}: {string.Join(", ", accessGroupd.ACLArray.Select(_ => _.Id))}");
                var workingGroups = new List<Group>();
                foreach (var acl in accessGroupd.ACLArray) {
                    workingGroups.AddRange(acl.ACEArray.Where(ace => groupMap.ContainsKey(ace.UserGrpId)).Select(ace => groupMap[ace.UserGrpId])
                        .Where(g => g.Name.StartsWith(config.GroupPrefix, StringComparison.InvariantCulture)));
                }

                // загружаем из Vault дочерние группы рабочих групп
                var queue = new Queue<Group>(workingGroups);
                while (queue.Any()) {
                    var g = queue.Dequeue();

                    if (workingGroups.All(_ => _.Id != g.Id)) {
                        workingGroups.Add(g);
                    }
                    var chilIds = wsm.AdminService.GetChildGroupIdsByGroupId(g.Id);
                    if (chilIds != null) {
                        foreach (var chilId in chilIds) {
                            queue.Enqueue(wsm.AdminService.GetGroupById(chilId));
                        }
                    }
                }

                context.Log($"Working groups: {string.Join(", ", workingGroups.Select(_ => _.Name))}", ACJE.MessageType.eInformation);

                // загружаем список пользователей входящих в дочерние группы
                var workingGroupUser = new Dictionary<long, User>();
                foreach (var workingGroup in workingGroups) {
                    var users = wsm.AdminService.GetMemberUsersByGroupId(workingGroup.Id);
                    if (users != null) {
                        foreach (var user in users) {
                            workingGroupUser[user.Id] = user;
                        }
                    }
                }
                context.Log($"Working group's users: {string.Join(", ", workingGroupUser.Values.Select(_ => _.Name))}", ACJE.MessageType.eInformation);

                if (configuredAccesses.WriteOnlyAccess) {
                    // если правило регламентирует доступ только для пользователей имещих право записи в каталог, в котором находится файл
                    // загружаем из Vault настройки безопастности
                    var acls = wsm.SecurityService.GetEntACLsByEntityIds(new[] {file.MasterId});
                    foreach (var acl in acls.ACLArray) {
                        context.Log($"acl {acl.Id}", ACJE.MessageType.eInformation);
                        foreach (var ace in acl.ACEArray) {
                            if (groupMap.TryGetValue(ace.UserGrpId, out var g)) {
                                context.Log($"\tace group {g.Name}: {string.Join(", ", ace.PermisArray.Select(_ => $"{_.Id}:{_.Val}"))}",
                                    ACJE.MessageType.eInformation);
                            } else if (workingGroupUser.TryGetValue(ace.UserGrpId, out var u)) {
                                context.Log($"\tace user {u.Name}: {string.Join(", ", ace.PermisArray.Select(_ => $"{_.Id}:{_.Val}"))}",
                                    ACJE.MessageType.eInformation);
                            } else {
                                context.Log($"\tace unkown {ace.UserGrpId}: {string.Join(", ", ace.PermisArray.Select(_ => $"{_.Id}:{_.Val}"))}",
                                    ACJE.MessageType.eInformation);
                            }
                        }
                    }
                    context.Log($"acls.EntACLArray: {string.Join(", ", acls.EntACLArray.Select(_ => $"{_.EntId} - {_.ACLId}"))}",
                        ACJE.MessageType.eInformation);

                    var userAce = acls.ACLArray.Length >= 2 ? acls.ACLArray[1].ACEArray : null;
                    var systemAce = acls.ACLArray.Length >= 1 ? acls.ACLArray[0].ACEArray : null;
                    // получаем эффективные права доступа пользователей
                    var aces = wsm.SecurityService.GetEffectivePermissions(workingGroupUser.Keys.ToArray(), userAce, systemAce);
                    if (aces != null) {
                        //собираем список пользователей с доступом на редактирование
                        foreach (var ace in aces) {
                            if (workingGroupUser.TryGetValue(ace.UserGrpId, out var user)) {
                                context.Log($"Working group's users access: {user.Name} {string.Join(", ", ace.PermisArray.Select(_ => $"{_.Id}:{_.Val}"))}",
                                    ACJE.MessageType.eInformation);
                                if (ace.PermisArray.Any(_ => _.Id == 1 && _.Val) &&
                                    ace.PermisArray.Any(_ => _.Id == 2 && _.Val)) {
                                    usersToNotify.Add(user, ace.PermisArray.Any(_ => _.Id == 2 && _.Val));
                                }
                            }
                        }
                    }
                } else {
                    // иначе собираем всех пользователей в групах имеющих доступ по настроенному правилу
                    foreach (var configuredAccessesGroup in configuredAccesses.Groups) {
                        var users = new List<User>();
                        _collectGroupUsers(wsm, configuredAccessesGroup.Id, users);

                        context.Log($"Users of configured access group '{configuredAccessesGroup.Name}': {string.Join(", ", users.Select(_ => _.Name))}",
                            ACJE.MessageType.eInformation);
                        foreach (var user in users) {
                            if (workingGroupUser.ContainsKey(user.Id)) {
                                usersToNotify.Add(user, true);
                            }
                        }
                    }
                }

                context.Log($"Users to notify: {string.Join(", ", usersToNotify.Keys.Select(_ => _.Name))}", ACJE.MessageType.eInformation);

                // получаем пиктограмму файла
                var propDef = context.Connection.PropertyManager.GetPropertyDefinitionBySystemName(PropertyDefinitionIds.Server.ThumbnailSystem);
                var fis = context.Connection.FileManager.GetLatestFilesByIterationIds(new[] {file.Id});
                var thumbnail = string.Empty;
                if (fis.Any()) {
                    var thumbnailInfo =
                        context.Connection.PropertyManager.GetPropertyValue(fis.Values.First(), propDef,
                            new PropertyValueSettings {UseLargeVaultStatusIcons = false}) as ThumbnailInfo;
                    if (thumbnailInfo != null) {
                        thumbnail = Convert.ToBase64String(thumbnailInfo.Image);
                    }
                }

                // генерируем перманентную ссылку на файл
                var permanentLink = string.Empty;
                try {
                    var serverUri = new Uri(wsm.InformationService.Url);
                    var ids = wsm.KnowledgeVaultService.GetPersistentIds(EntityClassIds.Files, new[] {file.Id}, EntPersistOpt.Latest);
                    var id = ids.FirstOrDefault() ?? string.Empty;
                    id = id.TrimEnd('=');
                    permanentLink = string.Format(
                        $"{serverUri.Scheme}://{serverUri.Host}/AutodeskTC/{serverUri.Host}/{context.Connection.Vault}#/Entity/Details?id=m{id}=&itemtype=File");
                } catch (Exception e) {
                    context.Log($"Возникла ошибка при генерации ссылки на файл:{Environment.NewLine}{e}", ACJE.MessageType.eInformation);
                }

                // получаем пользователя, который внес изменения в файл
                var userEditor = wsm.AdminService.GetUserByUserId(file.CreateUserId);

                // получаем название проекта
                var projectName = folder.FullName.ParseProjectName();

                // получает название участка
                var land = folder.FullName.ParseLand();

                // отправляем письма
                foreach (var user in usersToNotify.Distinct(new UserComparer())) {
                    // исключаем пользователя, который внес изменения в файл
                    if (user.Key.Id == userEditor.Id) {
                        continue;
                    }
                    try {
                        var email = _getEmail(file, folder, user.Key, userEditor, projectName, land, thumbnail, user.Value, permanentLink);
                        var message = new MailMessage(config.FromEmail, user.Key.Email, config.EmailTitle, email) {IsBodyHtml = true};
                        message.Attachments.Add(new Attachment(
                            _generateAttachment(context.Connection.Server, context.Connection.Vault, file.Name, folder.FullName), file.Name + ".acr"));
                        var client = new SmtpClient(config.SmtpHost, config.SmtpPort);
                        if (!string.IsNullOrEmpty(config.SmtpUsername) &&
                            !string.IsNullOrEmpty(config.SmtpPassword)) {
                            client.Credentials = new NetworkCredential(config.SmtpUsername, config.SmtpPassword);
                        }
                        client.Send(message);
                    } catch (SmtpException smtpException) {
                        context.Log(smtpException,
                            $"Возникла ошибка при отправке пользователю '{user.Key.Name}' email на адрес '{user.Key.Email}':{Environment.NewLine}{smtpException.StatusCode} | {smtpException}");
                    } catch (Exception e) {
                        context.Log(e,
                            $"Возникла ошибка при отправке пользователю '{user.Key.Name}' email на адрес '{user.Key.Email}':{Environment.NewLine}{e}");
                    }
                }

                return ACJE.JobOutcome.Success;
            } catch (Exception e) {
                context.Log(e, $"Unknon exception occure\n{e}");
                return ACJE.JobOutcome.Failure;
            }
        }

        public bool CanProcess(string jobType) {
            Logger.Write($"Check job handler for: {jobType}");
            return jobType.Equals("bigroup.notifier.notify", StringComparison.InvariantCultureIgnoreCase);
        }

        //private void _configure(Connection connection) {

        //    var users = connection.WebServiceManager.AdminService.GetAllUsers();

        //    var selections = args.Context.CurrentSelectionSet;
        //    foreach (var selection in selections) {
        //        Debug.WriteLine($"{selection.Id} {selection.Label} {selection.TypeId.SelectionContext}");
        //        foreach (var ace in connection.WebServiceManager.SecurityService.GetEffectivePermissions(users.Select(_ => _.Id).ToArray(), null, null)) {
        //            Debug.WriteLine($"\t{ace.UserGrpId} {string.Join(", ", ace.PermisArray.Select(_ => $"{_.Id}: {_.Val}"))}");
        //        }
        //        var entAcLsByEntityIds = connection.WebServiceManager.SecurityService.GetEntACLsByEntityIds(new[] { selection.Id });
        //        foreach (var acl in entAcLsByEntityIds.ACLArray) {
        //            Debug.WriteLine($"{acl.Id} {string.Join(", ", acl.ACEArray.Select(_ => $"{_.UserGrpId}"))}");
        //        }
        //        foreach (var entAcl in entAcLsByEntityIds.EntACLArray) {
        //            Debug.WriteLine($"{entAcl.EntId} - {entAcl.ACLId}");
        //        }

        //        if (selection.TypeId == SelectionTypeId.File) {
        //            var file = connection.WebServiceManager.DocumentService.GetLatestFileByMasterId(selection.Id);
        //            var folder = connection.WebServiceManager.DocumentService.GetFolderById(file.FolderId);
        //            Debug.WriteLine($"{folder.FullName} {folder.FullUncName}");
        //        }

        //    }

        //    foreach (var vgroup in connection.WebServiceManager.AdminService.GetAllGroups()) {
        //        Debug.WriteLine($"{vgroup.Id} {vgroup.Name}");
        //    }


        //}

        public void OnJobProcessorStartup(ACJE.IJobProcessorServices context) {
        }

        public void OnJobProcessorShutdown(ACJE.IJobProcessorServices context) {
        }

        public void OnJobProcessorWake(ACJE.IJobProcessorServices context) {
        }

        public void OnJobProcessorSleep(ACJE.IJobProcessorServices context) {
        }

        private void _collectGroupUsers(WebServiceManager wsm, long groupId, List<User> userList) {
            var users = wsm.AdminService.GetMemberUsersByGroupId(groupId);
            if (users != null) {
                userList.AddRange(users);
            }
            var memberGroups = wsm.AdminService.GetMemberGroupsByGroupId(groupId);
            if (memberGroups != null) {
                foreach (var memberGroup in memberGroups) {
                    _collectGroupUsers(wsm, memberGroup.Id, userList);
                }
            }
        }

        /// <summary>
        ///     Генерирует файл-приложение, с помощью котрого можно открыть обновленный файл в Vault
        /// </summary>
        private Stream _generateAttachment(string server, string vault, string fileName, string folderFullName) {
            var stream = new MemoryStream();

            /* 
            <?xml version="1.0" encoding="utf-8"?>
            <ADM xmlns="http://schemas.autodesk.com/msd/plm/ExplorerAutomation/2004-11-01">
                <Server>localhost</Server>
                <Vault>BI-Group</Vault>
                <Operations>
                    <Operation ObjectType="File">
                        <ObjectID>$/Химки/2_Исходные данные для проектирования/2.7_План детальных планировок/2. План детальной планировки.pdf</ObjectID>
                        <Command>Select</Command>
                    </Operation>
                </Operations>
            </ADM>
             */

            var doc = XDocument.Parse(
                @"<?xml version='1.0' encoding='utf-8'?><ADM xmlns='http://schemas.autodesk.com/msd/plm/ExplorerAutomation/2004-11-01'><Server></Server><Vault></Vault><Operations><Operation ObjectType='File'><ObjectID></ObjectID><Command>Select</Command></Operation></Operations></ADM>");
            var ns = doc.Root.GetDefaultNamespace();
            doc.Root.Element(ns.GetName("Server")).Value = server;
            doc.Root.Element(ns.GetName("Vault")).Value = vault;
            doc.Root.Element(ns.GetName("Operations")).Element(ns.GetName("Operation")).Element(ns.GetName("ObjectID")).Value = folderFullName + "/" + fileName;
            doc.Save(stream);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        ///     Генерирует письмо
        /// </summary>
        private string _getEmail(File file,
            Folder folder,
            User user,
            User editor,
            string projectName,
            string land,
            string thumbnail,
            bool writeAccess,
            string permanentLink) {
            if (string.IsNullOrEmpty(_body)) {
                var dir = Path.GetDirectoryName(typeof(ProcessorJobHandler).Assembly.Location);
                var templateDir = dir + Path.DirectorySeparatorChar + "email_template";
                if (!Directory.Exists(templateDir)) {
                    throw new ApplicationException($"Не найден каталог с шаблоном письма: {templateDir}");
                }
                var indexPath = templateDir + Path.DirectorySeparatorChar + "index.html";
                if (!System.IO.File.Exists(indexPath)) {
                    throw new ApplicationException($"Не найден файл с шаблоном письма: {indexPath}");
                }
                _body = System.IO.File.ReadAllText(indexPath);
            }

            return _body.Replace("%TO_USERNAME%", user.FirstName).Replace("%TO_USERSURNAME%", user.LastName).Replace("%FROM_USERNAME%", editor.FirstName)
                .Replace("%FROM_USERSURNAME%", editor.LastName).Replace("%PROJECTNAME%", projectName).Replace("%LANDNAME%", land)
                .Replace("%DOCUMENTNAME%", file.Name).Replace("%DOCUMENTSTATE%", file.FileLfCyc.LfCycStateName)
                .Replace("%DOCUMENTREVISION%", file.FileRev.Label).Replace("%DOCUMENTVERSION%", file.VerNum.ToString())
                .Replace("%DOCUMENTCATEGORY%", file.Cat.CatName).Replace("%DOCUMENTCOMMENT%", file.Comm).Replace("%DOCUMENTFOLDER%", folder.Name)
                .Replace("%DOCUMENTFOLDERPATH%", folder.FullName).Replace("%DOCUMENTTHUMBNAIL%", thumbnail).Replace("%WRITEACCESS%", writeAccess ? "Да" : "Нет")
                .Replace("%THINCLIENTLINK%", permanentLink);
        }

        private class UserComparer : IEqualityComparer<KeyValuePair<User, bool>> {
            public bool Equals(KeyValuePair<User, bool> x, KeyValuePair<User, bool> y) {
                return x.Key.Id == y.Key.Id;
            }

            public int GetHashCode(KeyValuePair<User, bool> obj) {
                return obj.Key.Id.GetHashCode();
            }
        }

        /// <summary>
        ///     Правило доступа к каталогу
        /// </summary>
        private class FolderGroupAccess {
            public readonly List<Group> Groups = new List<Group>();
            public Folder Folder;
            public long FolderId;
            public bool WriteOnlyAccess;

            public static bool IsWriteOnly(string key) {
                return key.EndsWith("s");
            }

            public static long ParseFolderId(string key) {
                var match = Regex.Match(key, @"f(?<folderId>\d*)[g|s]");
                return long.Parse(match.Groups["folderId"].Value);
            }

            public static long ParseGroupId(string key) {
                var match = Regex.Match(key, @"g(?<groupId>\d*)");
                return long.Parse(match.Groups["groupId"].Value);
            }
        }
    }
}