﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WixSharp;
using WixSharp.CommonTasks;

namespace Setup {
    class Program {
        static void Main(string[] args) {
            Func<string, string> vault = name => @"C:\ProgramData\Autodesk\Vault 2018\Extensions\BiNotifier\" + name;
            var vaultDir = new Dir("Vault 2018", 
                    new Dir("Extensions", new Dir("BiNotifier", new DirFiles(vault("*")), new Dir("email_template", new File("index.html"))))
                );

            Func<string, string> app = name => @"..\Build\" + name;
            var configurationAppDir = new Dir("BI Notifier", new DirFiles(app("*.dll")), new DirFiles(app("*.pdb")), new Dir("ru", new DirFiles(app(@"ru\*"))),
                new File(app("ConfigurationApplication.exe"), new FileShortcut("BI Notifier", "%Desktop%")));

            var version = new Version(1, 1, 5, 0);
            var project = new Project("BI Notifier") {
                Version = version,
                GUID = new Guid("{72FC70DE-A5D7-462C-AF09-01BD65940E21}"),
                OutFileName = $"Setup BI Notifier {version}",
                OutDir = @"..\Installer",
                Platform = Platform.x64,
                Language = "ru-RU",
                MajorUpgradeStrategy = MajorUpgradeStrategy.Default,
                UI = WUI.WixUI_ProgressOnly,
                ControlPanelInfo = { ProductIcon = "icon.ico", Manufacturer = "BI-Group" },
            };
            project.AddDir(new Dir(@"%CommonAppDataFolder%\Autodesk", vaultDir));
            project.AddDir(new Dir(@"%ProgramFiles%\BI-Group", configurationAppDir));
            Compiler.BuildMsi(project);
        }
    }
}
