﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Autodesk.DataManagement.Client.Framework.Vault.Forms.Settings;
using ConfigurationApplication.Properties;
using ConfigurationApplication.UI;

namespace ConfigurationApplication {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        protected override void OnStartup(StartupEventArgs e) {
            base.OnStartup(e);

            var view = new View(new ViewModel(new GuiValutProvider(), Commons.ConfigurationSettings.Load()));
            this.MainWindow = view;
            this.ShutdownMode = ShutdownMode.OnMainWindowClose;

            view.Show();
        }
       
    }
}
