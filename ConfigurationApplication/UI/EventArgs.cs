using System;

namespace ConfigurationApplication.UI {
    public class EventArgs<T> : EventArgs {
        public T Argument { get; }

        public EventArgs(T argument) {
            Argument = argument;
        }
    }
}