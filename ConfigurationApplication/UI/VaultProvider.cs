using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// �������� ������ � ��������� � ������� �� Vault
    /// </summary>
    public abstract class VaultProvider: IVaultProvider {
        protected Connection _connection;
        public abstract void Login();

        public bool Connected => _connection != null;
        public abstract void Logout();

        public virtual FolderPresenter GetRootFolder(string path) {
            if (_connection == null) {
                throw new ApplicationException("���������� ����������� � Vault");
            }
            var folder = _connection.WebServiceManager.DocumentService.GetFolderByPath(path);
            if (folder == null) {
                throw new ApplicationException("���� � ��������� �������� ������ �����������");
            }
            return new FolderPresenter(folder, null);
        }

        public virtual IEnumerable<FolderPresenter> GetChildrenFolders(FolderPresenter parent) {
            if (_connection == null) {
                throw new ApplicationException("���������� ����������� � Vault");
            }
            var folders = _connection.WebServiceManager.DocumentService.GetFoldersByParentId(parent.Folder.Id, false);
            return folders?.Select(_ => new FolderPresenter(_, parent)) ?? Enumerable.Empty<FolderPresenter>() ;
        }

        public IEnumerable<GroupPresenter> GetGroups() {
            var groups = _connection.WebServiceManager.AdminService.GetAllGroups();
            return groups.Where(_ => _.IsActive).Select(_ => new GroupPresenter(_));
        }
    }
}