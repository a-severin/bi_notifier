using System;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// ������ ���������� �������
    /// </summary>
    public class ExceptionMessage {
        public string Context { get; }
        public Exception Exception { get; }

        public ExceptionMessage(string context, Exception exception) {
            Context = context;
            Exception = exception;
        }
    }
}