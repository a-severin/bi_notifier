using System.Collections.Generic;
using System.Collections.ObjectModel;
using Autodesk.Connectivity.WebServices;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// ��������� ������� � ��������
    /// </summary>
    public class FolderPresenter : Prism.Mvvm.BindableBase {
        private readonly FolderPresenter _parent;
        private bool _sendOnlyToEditAccess;
        public Folder Folder { get; }

        public FolderPresenter(Folder folder, FolderPresenter parent) {
            _parent = parent;
            Folder = folder;
        }

        public void CopyAccessFromParent() {
            if (_parent == null) {
                return;
            }
            SendOnlyToEditAccess = _parent.SendOnlyToEditAccess;
            for (var i = 0; i < GroupAccessPresenters.Count; i++) {
                GroupAccessPresenters[i].HasAccess = _parent.GroupAccessPresenters[i].HasAccess;
            }
        }

        public ObservableCollection<FolderPresenter> Children { get; } = new ObservableCollection<FolderPresenter>();
        public IReadOnlyList<GroupAccessPresenter> GroupAccessPresenters { get; set; }
        public string Name => Folder.Name;

        public bool SendOnlyToEditAccess {
            get => _sendOnlyToEditAccess;
            set => SetProperty(ref _sendOnlyToEditAccess, value);
        }
    }
}