using System.Collections.Generic;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// ���������� �������� ��� ������ � Vault
    /// </summary>
    public interface IVaultProvider {
        void Login();
        bool Connected { get; }
        void Logout();
        FolderPresenter GetRootFolder(string path);
        IEnumerable<FolderPresenter> GetChildrenFolders(FolderPresenter parent);
        IEnumerable<GroupPresenter> GetGroups();
    }
}