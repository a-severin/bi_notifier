using Autodesk.DataManagement.Client.Framework.Vault.Forms;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// ���������� ����������� � Vault ����� ������� ������
    /// </summary>
    public class GuiValutProvider : VaultProvider {

        public override void Login() {
            Library.Initialize();
            var result = Library.Login2(null);
            if (result != null) {
                if (result.Exception != null) {
                    throw result.Exception;
                }
                _connection = result.Connection;
            }
        }

        public override void Logout() {
            if (_connection != null && Autodesk.DataManagement.Client.Framework.Vault.Library.ConnectionManager.LogOut(_connection)) {
                _connection = null;
            }
        }
    }
}