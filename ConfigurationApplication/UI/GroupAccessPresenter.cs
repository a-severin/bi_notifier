using Autodesk.Connectivity.WebServices;
using Prism.Mvvm;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// ������� ������� ������ � ��������
    /// </summary>
    public class GroupAccessPresenter : BindableBase {
        private bool _hasAccess;

        public GroupAccessPresenter(Group vgroup) {
            Vgroup = vgroup;
        }

        public bool HasAccess {
            get => _hasAccess;
            set => SetProperty(ref _hasAccess, value);
        }

        public string Name => Vgroup.Name;
        public Group Vgroup { get; }
    }
}