﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Commons;
using Prism.Commands;
using Prism.Mvvm;

namespace ConfigurationApplication.UI {
    public class ViewModel : BindableBase {
        private readonly List<FolderPresenter> _folderPresenterList = new List<FolderPresenter>();
        private readonly IVaultProvider _vaultProvider;
        private ExceptionMessage _exceptionMessage;
        private ObservableCollection<FolderPresenter> _folderPresenters;
        private FolderPresenter _selectedFolder;

        public ViewModel(IVaultProvider vaultProvider, ConfigurationSettings settings) {
            _vaultProvider = vaultProvider;
            Settings = settings;

            if (Settings.SelectedGroups == null) {
                Settings.SelectedGroups = new StringCollection();
            }

            if (Settings.GroupAccess == null) {
                Settings.GroupAccess = new StringCollection();
            }

            _initCommands();
        }

        public DelegateCommand Connect { get; private set; }

        public ExceptionMessage ExceptionMessage {
            get => _exceptionMessage;
            set {
                SetProperty(ref _exceptionMessage, value);
                ExceptionMessageChanged?.Invoke(this, new EventArgs<ExceptionMessage>(_exceptionMessage));
            }
        }

        /// <summary>
        /// Коллекция настроек доступа к каталогу
        /// </summary>
        public ObservableCollection<FolderPresenter> FolderPresenters {
            get => _folderPresenters;
            set {
                SetProperty(ref _folderPresenters, value);
                FolderPresentersChanged?.Invoke(this, new EventArgs<IReadOnlyCollection<FolderPresenter>>(_folderPresenters));
            }
        }

        public ObservableCollection<GroupPresenter> GroupPresenters { get; } = new ObservableCollection<GroupPresenter>();

        public DelegateCommand LoadFolders { get; private set; }

        /// <summary>
        /// Считывает и записывает путь к корневому каталогу в настройки
        /// </summary>
        public string RootPath {
            get => Settings.RootPath;
            set {
                Settings.RootPath = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Сохраняет изменения настроек
        /// </summary>
        public DelegateCommand Save { get; private set; }

        /// <summary>
        /// Выделенные натсройки доступа к каталогу
        /// </summary>
        public FolderPresenter SelectedFolder {
            get => _selectedFolder;
            set => SetProperty(ref _selectedFolder, value);
        }

        /// <summary>
        /// Копирует настройки достпа к каталогу из выделенного
        /// </summary>
        public Prism.Commands.DelegateCommand CopyFromParent { get; private set; }

        /// <summary>
        /// Настройки приложения
        /// </summary>
        public ConfigurationSettings Settings { get; }

        /// <summary>
        /// Ошибка про выполнении команды приложения
        /// </summary>
        public event EventHandler<EventArgs<ExceptionMessage>> ExceptionMessageChanged;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<EventArgs<IReadOnlyCollection<FolderPresenter>>> FolderPresentersChanged;

        public void Cleanup() {
            _vaultProvider.Logout();
        }

        private void _initCommands() {
            CopyFromParent = new DelegateCommand(() => {
                if (SelectedFolder == null) {
                    return;
                }
                SelectedFolder.CopyAccessFromParent();
            }, () => SelectedFolder != null).ObservesProperty(() => SelectedFolder);

            Save = new DelegateCommand(() => {
                if (!Settings.RootPath.Equals(RootPath, StringComparison.InvariantCulture)) {
                    Settings.RootPath = RootPath;
                }
                if (GroupPresenters != null) {
                    Settings.SelectedGroups.Clear();
                    Settings.SelectedGroups.AddRange(GroupPresenters.Where(_ => _.Apply).Select(_ => _.Vgroup.Id.ToString()).ToArray());
                }
                if (FolderPresenters != null) {
                    Settings.GroupAccess.Clear();
                    foreach (var folder in _folderPresenterList) {
                        if (folder.SendOnlyToEditAccess) {
                            Settings.GroupAccess.Add($"f{folder.Folder.Id}s");
                        }
                        foreach (var groupAccessPresenter in folder.GroupAccessPresenters.Where(_ => _.HasAccess)) {
                            Settings.GroupAccess.Add($"f{folder.Folder.Id}g{groupAccessPresenter.Vgroup.Id}");
                        }
                    }
                }

                Settings.Save();
            }, () => _vaultProvider.Connected);

            Connect = new DelegateCommand(() => {
                try {
                    _vaultProvider.Login();
                } catch (Exception e) {
                    ExceptionMessage = new ExceptionMessage("Возникла ошибка при подключении", e);
                }
                Connect.RaiseCanExecuteChanged();
                LoadFolders.RaiseCanExecuteChanged();
                Save.RaiseCanExecuteChanged();
            }, () => _vaultProvider.Connected == false);

            LoadFolders = new DelegateCommand(() => {
                try {
                    if (GroupPresenters.Count == 0) {
                        var savedSelection = new HashSet<long>(Settings.SelectedGroups.Cast<string>().Select(long.Parse));
                        foreach (var vgroup in _vaultProvider.GetGroups().Where(_ => !_.Vgroup.Name.StartsWith(Settings.GroupPrefix)).OrderBy(_ => _.Name)) {
                            if (savedSelection.Contains(vgroup.Vgroup.Id)) {
                                vgroup.Apply = true;
                            }
                            GroupPresenters.Add(vgroup);
                        }
                    }

                    IReadOnlyList<GroupAccessPresenter> accessPresenters(FolderPresenter folder) {
                        if (Settings.GroupAccess.Contains($"f{folder.Folder.Id}s")) {
                            folder.SendOnlyToEditAccess = true;
                        }
                        var list = new List<GroupAccessPresenter>();
                        foreach (var groupPresenter in GroupPresenters.Where(_ => _.Apply)) {
                            var groupAccessPresenter = new GroupAccessPresenter(groupPresenter.Vgroup);
                            if (Settings.GroupAccess.Contains($"f{folder.Folder.Id}g{groupPresenter.Vgroup.Id}")) {
                                groupAccessPresenter.HasAccess = true;
                            }
                            list.Add(groupAccessPresenter);
                        }
                        return list;
                    }

                    _folderPresenterList.Clear();
                    var queue = new Queue<FolderPresenter>();

                    var root = _vaultProvider.GetRootFolder(RootPath);
                    root.GroupAccessPresenters = accessPresenters(root);
                    _folderPresenterList.Add(root);

                    queue.Enqueue(root);
                    var addedFolders = new HashSet<long> {root.Folder.Id};
                    while (queue.Any()) {
                        var folder = queue.Dequeue();
                        foreach (var childrenFolder in _vaultProvider.GetChildrenFolders(folder)) {
                            if (addedFolders.Add(childrenFolder.Folder.Id)) {
                                childrenFolder.GroupAccessPresenters = accessPresenters(childrenFolder);
                                queue.Enqueue(childrenFolder);
                                folder.Children.Add(childrenFolder);
                                _folderPresenterList.Add(childrenFolder);
                            }
                        }
                    }

                    FolderPresenters = new ObservableCollection<FolderPresenter> {root};
                } catch (Exception e) {
                    Debug.WriteLine(e);
                    ExceptionMessage = new ExceptionMessage("Возникла ошибка при загрузке дерева каталогов", e);
                }
            }, () => _vaultProvider.Connected && !string.IsNullOrEmpty(RootPath)).ObservesProperty(() => RootPath);
        }
    }
}