﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Syncfusion.UI.Xaml.TreeGrid;

namespace ConfigurationApplication.UI {
    /// <summary>
    ///     Interaction logic for View.xaml
    /// </summary>
    public partial class View {
        public View(ViewModel vm) {
            DataContext = vm;
            InitializeComponent();
            // обеспечим отчистки ресурсов при закрытии приложения
            Closed += (sender, args) => {
                vm.Cleanup();
            };

            // инициализируем приложение после загрузки
            Loaded += (sender, args) => {
                vm.Connect.Execute();
                vm.LoadFolders.Execute();
            };

            vm.FolderPresentersChanged += _showFolders;
            vm.ExceptionMessageChanged += _showException;
        }

        /// <summary>
        /// Показывает ошибки при выполнении команд
        /// </summary>
        private void _showException(object o, EventArgs<ExceptionMessage> args) {
            MessageBox.Show($"{args.Argument.Context}:\n{args.Argument.Exception.Message}", Title, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Динамически загружает столбцы в конфигураторе доступа к каталогам
        /// </summary>
        private void _showFolders(object sender, EventArgs<IReadOnlyCollection<FolderPresenter>> args) {
            FoldersGrid.Columns.Clear();

            var folders = args.Argument;

            if (folders == null ||
                !folders.Any()) {
                return;
            }
            var folder = folders.First();

            FoldersGrid.Columns.Add(new TreeGridTextColumn {
                HeaderText = "Каталог",
                MappingName = nameof(FolderPresenter.Name),
                ColumnSizer = TreeColumnSizer.SizeToCells,
                AllowResizing = true
                
            });
            FoldersGrid.Columns.Add(new TreeGridCheckBoxColumn {
                HeaderText = "Отправлять только с\nправами на изменение",
                MappingName = nameof(FolderPresenter.SendOnlyToEditAccess),
                ColumnSizer = TreeColumnSizer.SizeToHeader,
                AllowEditing = true,
                AllowResizing = true
            });


            if (folder.GroupAccessPresenters != null) {
                for (var i = 0; i < folder.GroupAccessPresenters.Count; i++) {
                    FoldersGrid.Columns.Add(new TreeGridCheckBoxColumn {
                        HeaderText = folder.GroupAccessPresenters[i].Name,
                        MappingName = $"{nameof(FolderPresenter.GroupAccessPresenters)}[{i}].{nameof(GroupAccessPresenter.HasAccess)}",
                        ColumnSizer = TreeColumnSizer.SizeToHeader,
                        AllowEditing = true,
                        AllowResizing = true
                    });
                }
            }
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e) {
            if (this.DataContext is ViewModel vm && sender is PasswordBox pwd) {
                vm.Settings.SmtpPassword = pwd.Password;
            }
        }
    }
}