using Autodesk.Connectivity.WebServices;

namespace ConfigurationApplication.UI {
    /// <summary>
    /// ��������� �������� ������
    /// </summary>
    public class GroupPresenter : Prism.Mvvm.BindableBase {
        public Group Vgroup { get; }
        private bool _apply;

        public GroupPresenter(Group vgroup) {
            Vgroup = vgroup;
        }

        public string Name => Vgroup.Name;

        public bool Apply {
            get => _apply;
            set => SetProperty(ref _apply, value);
        }

        public override string ToString() {
            return Name;
        }
    }
}